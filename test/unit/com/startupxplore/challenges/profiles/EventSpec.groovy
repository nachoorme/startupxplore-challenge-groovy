package com.startupxplore.challenges.profiles

import grails.test.mixin.*
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Event)
class EventSpec extends ConstraintUnitSpec  {

    def setup() {        
        mockForConstraintsTests(Event, [new Event(id: '123456789')])
    }
    
    @Unroll("test event constraint #field is #error")
    def "test event constraint"() {
        when:
        def obj = new Event("$field": val)

        then:
        validateConstraints(obj, field, error)

        where:
        error                  | field          | val
        'nullable'             | 'name'         | null
        'nullable'                | 'name'         | ''
        'maxSize'                 | 'description'  | getLongString(145)
        'blank'                | 'id'        | ''
        'unique'               | 'id'        | '123456789'
        'maxSize'                 | 'overview'  | getLongString(4200)
    }    
   
}
