package com.startupxplore.dto.notifications




class Notification {
    NotificationType type
    
    
    //User who is notified
    String userIdAsObject
    
    String recipientEmailAddress
    String recipientName
    String link
    
    String subject
    String body
    String locale = "es"
    
    
    static Notification buildWelcomeMail(userIdAsObject, String locale){
        
        Notification n = new Notification(type:NotificationType.WELCOMEMAIL,userIdAsObject:userIdAsObject)
        if (locale){
            n.locale=locale
        }
        return n
    }
    
    static Notification buildFromSSM(userIdAsObject,String link, String locale){
              
        Notification n = new Notification(type:NotificationType.FROMSSM,userAsObject:userIdAsObject,link:link)
        if (locale){
            n.locale=locale
        }
        return n
    }
    
    static Notification buildConfirmationLinkReminder(userIdAsObject, String link, String locale){
       
        Notification n = new Notification(type:NotificationType.CONFIRMATIONLINKREMINDER,userIdAsObject:userIdAsObject,link:link)
        if (locale){
            n.locale=locale
        }
        return n
    }
    
   
    
    static Notification buildResetPwd(userIdAsObject, String link, String locale){
       
        Notification n = new Notification(type:NotificationType.RESETPWD,userAsObject:userIdAsObject,link:link)
        if (locale){
            n.locale=locale
        }
        return n
    }
    
   def getRecipientAddressFromUserId(){
       this.recipientEmailAddress = "${this.userIdAsObject}@gmail.com"
       this.recipientName = "My name is ${this.userIdAsObject}"
   }
    
    EmailNotification toEmailNotification(){
        EmailNotification result = new EmailNotification()
       
        getRecipientAddressFromUserId()
        
        switch (type){
            
            case NotificationType.WELCOMEMAIL:                
                result.from="Javier Megias - Startupxplore<javier@startupxplore.com>"
                result.to=this.recipientEmailAddress               
                result.viewModel = [name:(this.recipientName ? this.recipientName : "Somebody"),link:link]
                result.subject = "Welcome to startupxplore"
                result.headers = ["X-MC-Template":"welcome-mail-${locale}","X-MC-MergeVars": """ ${result.viewModel} """]
                break;
            
            case NotificationType.CONFIRMATIONLINKREMINDER:                
                result.to=this.recipientEmailAddress
                result.viewModel = [name:(this.recipientName ? this.recipientName : "Somebody"),link:link]
                result.subject = "This is your confirmation link"
                result.headers = ["X-MC-Template":"confirmationlinkreminder-${locale}","X-MC-MergeVars": """ ${result.viewModel} """]
                break;
            
            case NotificationType.FROMSSM:                
                result.from="Javier Megias - Startupxplore<javier@startupxplore.com>"
                result.to=this.recipientEmailAddress
                result.viewModel = [name:(this.recipientName ? this.recipientName : "Somebody"),link:link]
                result.subject = "Welcome from SSM"
                result.headers = ["X-MC-Template":"from-ssm-${locale}","X-MC-MergeVars": """ ${result.viewModel} """]
                break;
            
            
            case NotificationType.RESETPWD:
                result.from = "startupxplore<info@startupxplore.com>"               
                result.to=this.recipientEmailAddress
                result.viewModel = [name:(this.recipientName ? this.recipientName : "Somebody"),link:link]
                result.subject = "Press the link to reset your password"
                result.headers = ["X-MC-Template":"reset-pwd-${locale}","X-MC-Autotext":"true","X-MC-Important":"true","X-MC-MergeVars": """ ${result.viewModel} """]
                break;
            
                 
        }
        
        
        return result;
    }
    
    @Override
    public String toString() {
        return "Notification [locale=$locale ,type=$type, userAsObject= $userIdAsObject,"+
                ", link=$link]";
    }
    
    void dispatch(){
        println "Before sending notification: ${this}"
        EmailNotification emailNotification = this.toEmailNotification()
        println "Email to send: ${emailNotification}"
    }
    
}
