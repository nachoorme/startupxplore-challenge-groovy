package com.startupxplore.dto.notifications

class EmailNotification {
	
	static final DEFAULT_ADDRESS = "info@startupxplore.com"

    def from = "The Startupxplore Team <info@startupxplore.com>" 
	def to = DEFAULT_ADDRESS
	def cc = DEFAULT_ADDRESS
	def bcc = DEFAULT_ADDRESS

    def headers
	String subject = ""
	String view = ""
	String body = ""
	
	def viewModel = [:]
	
	def attachBytes = null

	String toString(){
		"From: $from - to:${to} - subject:$subject - view:$view - viewModel:$viewModel - headers:$headers"
	}
	
}
