package com.startupxplore.challenges

import groovy.xml.*

class TreesController {
    
    /*
     *   1
        / \
       /   \
      /     \
     2       3
    / \     /
   4   5   6
  /       / \
 7       8   9
     *
     */
    
    def binaryXMLTree = '''
    <uno>
        <dos>
            <cuatro><siete/></cuatro>
            <cinco/>
        </dos>
        <tres>
            <seis><ocho/><nueve/></seis>
        </tres>
    </uno>
'''
    
    def preorder() {
        
        render  "1 2 4 7 5 3 6 8 9"
        render  "<br/>"
        render "your solution here"
    }
    
    def inorder()  {
        render  "7 4 2 5 1 8 6 9 3"
        render  "<br/>"
        render "your solution here"
    }
    
    def postorder(){
        render  "7 4 5 2 8 9 6 3 1"
        render  "<br/>"
        render "your solution here"
    }
    
    def levelorder(){
        render "1 2 3 4 5 6 7 8 9"
        render  "<br/>"
        render "your solution here"
    }
}
