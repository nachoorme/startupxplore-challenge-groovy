package com.startupxplore.challenges.profiles





class Asset {

	
	
	/* attributes */ 
	String id
	String name
	String type	
	String overview
	String url
	
	
    static constraints = {
		id	unique: true
		name     nullable:true
		overview  nullable:false
		url       nullable:true, url:true
		
		
		organization nullable:true
		person	nullable:true
		event	nullable:true		
    }
	
	
	
	/* hasMany / belongsTo */
	
	static belongsTo = [organization: Organization, person:Person, event:Event]
	
	
	String toString(){
		"$name"
	}
	
	String getAssetURL(){
        url
    }
	
}
