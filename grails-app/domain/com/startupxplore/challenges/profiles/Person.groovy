package com.startupxplore.challenges.profiles

import grails.web.JSONBuilder



class Person {

	
	
	String id
	String name
	String type
	String status 
    	
	Boolean starred = false
	
	String description
	String overview
		
	Date dateCreated
	Date lastUpdated
	
		
	String permalink = ""
	String startupxploreUrl
	String homepageUrl
	String blogUrl
	String blogFeedUrl
	String facebookUrl
	String twitterUrl
	String googleUrl
	String linkedinUrl
	String pinterestUrl
	String youtubeUrl
	String emailAddress
	String phoneNumber
    
	
	
    static constraints = {
        id              unique: true
        name            nullable:false, blank:false
        description     nullable:true, maxSize:140
        overview        nullable: true, maxsize:4000, widget:'textarea'
        
    }
    
    static mapping = {
        id generator: 'uuid'
        overview type: 'text'
    }

	
	
	/* hasMany / belongsTo */
	
	static hasMany = [assets:Asset]
	
	/* Custom methods */
	String toString(){
		"$name"
	}
	
	String toJson(){	
					
		JSONBuilder builder = new JSONBuilder()
			
		return  builder.build {
						this.properties.each {propName, propValue ->
								setProperty(propName, propValue)
						}
						
					}
		}
	
	
	/* Asset management */
	
	static Asset getProfileImageFromId(String personId){
		Person person = Person.get(personId)
		return person.getProfileImage()
	}
	
	Asset getProfileImage(){
		def assets = getAllAssets("PROFILE");
		
		assets[0]
	}
	
	def getAllAssets(type){
		
		def assets = Asset.findAllByOrganizationAndType(this,type,[fetch:[organization:'join']]);
	}
	
	
	
}
